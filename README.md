A plugin for CtxSearch (https://gitlab.com/idnael/ctxsearch)
to search for lego parts or lego sets in Bricklink.com
It shows a preview image in the menu. If user selects it, opens the corresponding bricklink page.

![](20220409_screenshot_bricklink.png)