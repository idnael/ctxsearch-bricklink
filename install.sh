#!/bin/bash

op="$1"

if [ "$op" == "--remove" ]
then
  sudo rm -rf /usr/share/ctxsearch/plugins/plugin_bricklink*
else

  # go to folder where this script is
  cd `dirname "$0"`

  name=ctxsearch-bricklink-0.20
  target=_deb/$name

  sudo rm -rf $target

  mkdir -p $target
  mkdir -p $target/DEBIAN
  mkdir -p $target/usr/share/ctxsearch/plugins

  cp -rp plugins $target/usr/share/ctxsearch

  # ficherios especiais debian
  cp -rp etc/debian.control $target/DEBIAN/control

  sudo chown root:root -R $target/

  if [ "$op" == "--install" ]
  then
    sudo cp -rp $target/usr /

    sudo rm -rf $target

  else
    if [ "$op" == "--debian" ]
    then
      # this will create a deb file in _deb folder
      dpkg -b $target/
      sudo rm -rf $target
    fi
  fi
fi
