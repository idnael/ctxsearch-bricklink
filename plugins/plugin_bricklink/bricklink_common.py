
import yaml

try:
    from bricklink_api.auth import oauth
    from bricklink_api.catalog_item import get_price_guide, Type, NewOrUsed, get_item_image, get_item
except:
    BRICKLINKAPI_INSTALL_COMMAND = "sudo pip3 install git+git://github.com/BrickBytes/bricklink_api.git@master"

    raise Exception(f"Please install Bricklink api with {BRICKLINKAPI_INSTALL_COMMAND}")

TEST_PARTID = "3005"

AUTH_FILE_HELP = """Auth file is an yaml file with bricklink authentication info. Example:
consumer_key: aaaaaaaaaaaaaaaaaaaaa
consumer_secret: aaaaaaaaaaaaaaaaaaaaa
tokens:
- token_value: aaaaaaaaaaaaaaaaaaaaa
  token_secret: aaaaaaaaaaaaaaaaaaaaa
- token_value: aaaaaaaaaaaaaaaaaaaaa
  token_secret: aaaaaaaaaaaaaaaaaaaaa
"""

DANIEL_BRICKLINK_CREDENTIALS_FILE = "/home/daniel/etc/lego/documentos/bricklink_credentials.yaml"

# Creates and returns oauth obj based on auth_file yaml file
# returns a requests_oauthlib.oauth1_auth.OAuth1
# raises error if can't connect or if file missing, or with wrong format
def bricklink_auth(auth_file = DANIEL_BRICKLINK_CREDENTIALS_FILE):
    try:
        with open(auth_file, "r") as f:
            authinfo = yaml.load(f, Loader=yaml.FullLoader)

    except:
        raise Exception(f"Error in {auth_file}")

    consumer_key = authinfo["consumer_key"]
    consumer_secret = authinfo["consumer_secret"]

    for token in authinfo["tokens"]:
        token_value = token["token_value"]
        token_secret = token["token_secret"]

        auth = oauth(consumer_key, consumer_secret, token_value, token_secret)

        # call a service just to check if connection is ok:
        answer = get_item(Type.PART, TEST_PARTID, auth=auth)

        if answer["meta"]["code"] == 200:
            return auth

    raise Exception("Can't connect to bricklink")

