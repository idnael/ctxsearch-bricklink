
# Para instalar:
# ln -s ~/etc/lego/comp/ctxsearch_plugins/plugin_bricklink.py ~/.config/CtxSearch/plugins/

import re, logging, requests, tempfile, urllib, os
from threading import Thread

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib,GdkPixbuf, GLib

from ctxsearch.plugin_base import PluginBase
from .bricklink_common import bricklink_auth, AUTH_FILE_HELP

import bricklink_api

# 3005= brick 1x1
# some old parts that start with letters, like "sc002"
# outro exemplo invulgar: 78c05 -- nao detectado!
LEGOID_RE = re.compile("\\b[a-z]{,3}[0-9]{2,}[a-z]?(-?[0-9a-z]+)?\\b")

MAX_IMAGE_SIZE = 200

class PrepareMenuWorker:
    # init should be called in UI thread
    # add_menuitem_callback is to be called to add a menu item to the ctxmenu
    # When user clicks the item, clicked_callback should be called
    def __init__(self, ctxsearch, auth, ctx, action, add_menuitem_callback):
        logging.debug("PrepareMenuWorker.init")
        self.ctxsearch = ctxsearch
        self.auth = auth

        self.ctx = ctx
        self.action = action

        self.add_menuitem_callback = add_menuitem_callback

        self.interrupted = False

        # show progressbar while loading
        progress = Gtk.ProgressBar()
        progress.pulse()
        # progress = Gtk.Spinner()

        self.menuitem0 = Gtk.MenuItem()
        self.menuitem0.add(progress)
        self.menuitem0.show_all()
        self.add_menuitem_callback(self.menuitem0)

        Thread(target=self.doit).start()

    # this will be called in a subthread, not gtk thread
    def doit(self):

        self.load_item(bricklink_api.catalog_item.Type.PART, self.ctx["lego_id"])

        if not self.load_item(bricklink_api.catalog_item.Type.SET, self.ctx["lego_id"]):
            # sometimes and old id was added a "-1" because Lego reuses id... only for sets?
            self.load_item(bricklink_api.catalog_item.Type.SET, self.ctx["lego_id"] + "-1")

        # after work done, remove progressbar
        GLib.idle_add(self.menuitem0.destroy)

    # load item image and if suceeds, creates menuitem
    # returns True if suceeded
    # should be called outside UI thread
    # TODO verificar erros
    def load_item(self, type, lego_id):
        if self.interrupted: return

        answer = bricklink_api.catalog_item.get_item(type, lego_id, auth=self.auth)
        if self.interrupted: return

        logging.debug(f"_load_item {lego_id} type={type} answer={answer}")

        data = answer.get("data", {})

        if "image_url" in data:
            if "name" in data:
                item_name = data["name"]
            else:
                item_name = None

            image_url = "https:" + data["image_url"]

            if type == bricklink_api.catalog_item.Type.PART:
                click_url = f"https://www.bricklink.com/v2/catalog/catalogitem.page?P={lego_id}"
            else:
                click_url = f"https://www.bricklink.com/v2/catalog/catalogitem.page?S={lego_id}"

            logging.debug(f"type={type}, id={lego_id}, image_url={image_url}, click_url={click_url}")

            split_url = urllib.parse.urlsplit(image_url)
            file_ext = os.path.splitext(split_url.path)[1]
            tmpfile =tempfile.mkstemp(suffix= "."+file_ext)[1]

            # TODO como interromper?

            response = requests.get(image_url, stream=True)
            with open(tmpfile, "wb") as fd:
                for chunk in response.iter_content(1024):
                    if self.interrupted: return

                    fd.write(chunk)

            if self.interrupted: return

            # TODO: como criar Gtk.Image ou Gdk.Pixbuf apartir da stream?
            # existe o Gdk.Pixbuf.new_from_stream que recebe um Gio.InputStream, mas nao sei como obter apartir dum url

            pixbuf = GdkPixbuf.Pixbuf.new_from_file(tmpfile)
            if pixbuf.get_width() > MAX_IMAGE_SIZE or pixbuf.get_height() > MAX_IMAGE_SIZE:
                scale = min(MAX_IMAGE_SIZE / pixbuf.get_width(), MAX_IMAGE_SIZE / pixbuf.get_height())
                w = pixbuf.get_width() * scale
                h = pixbuf.get_height() * scale

                pixbuf = pixbuf.scale_simple(w, h, GdkPixbuf.InterpType.NEAREST)

            if type == bricklink_api.catalog_item.Type.PART:
                label = "Lego part " + lego_id
            else:
                label = "Lego set " + lego_id

            if item_name:
                label += "\n"+item_name

            # change the menu in UI thread:
            GLib.idle_add(self.add_menuitem, pixbuf, label, click_url, self.add_menuitem_callback)

            return True

        return False

    def add_menuitem(self, pixbuf, label, click_url, add_menuitem_callback):

        img = Gtk.Image.new_from_pixbuf(pixbuf)

        hbox = Gtk.HBox()
        hbox.pack_start(img, expand=False, fill=False, padding=0)

        vbox = Gtk.VBox()
        # vbox.pack_start(Gtk.Label(label=label), expand=False, fill=False, padding=0)
        # vbox.pack_start(hbox, expand=False, fill=False, padding=0)
        vbox.add(Gtk.Label(label=label))
        vbox.add(hbox)

        mi = Gtk.MenuItem()
        mi.add(vbox)
        mi.show_all()
        #mi.connect('button-release-event', self.clicked_callback, click_url, action)
        self.ctxsearch.prepare_click_to_open_url(mi, click_url, self.ctx, self.action)

        add_menuitem_callback(mi)

    def kill(self):
        logging.debug("PrepareMenuWorker.kill")
        self.interrupted = True

class BricklinkPlugin(PluginBase):
    auth = None
    worker = None

    # identifies a legoid in the text, put's it in the context
    def prepare_context(self, ctx):
        m = LEGOID_RE.search(ctx["text"])
        if m:
            ctx["lego_id"] = m.group(0)
        else:
            ctx["lego_id"] = None

    def connect(self):
        try:
            auth_file = self.config.get("bricklink")["auth_file"]
            logging.debug("auth_file:"+auth_file)
        except:
            raise Exception("No bricklink config found")

        # this will raises error if can't connect
        self.auth = bricklink_auth(auth_file)

    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if "bricklink" not in action:
            return

        # if not None...
        if not ctx.get("lego_id"):
            return

        logging.debug("prepare_menu")

        if not self.auth:
            # try to connect
            # isso vai ser lento, e nao deveria estar no prepare_menu de forma sincrona. Mas apenas acontece a 1º vez
            self.connect()

        if not self.auth:
            return

        if self.worker:
            self.worker.kill()
            self.worker = None

        self.worker = PrepareMenuWorker(self.ctxsearch, self.auth, ctx, action, add_menuitem_callback)

    def menu_destroyed(self):
        logging.debug("menu_destroyed")
        if self.worker:
            self.worker.kill()
            self.worker = None


    def add_default_config(self, config):
        if "bricklink" not in config.data:
            logging.debug("add_default_config")

            config.data["bricklink"] = {"auth_file":""}

            # this will add a multi line comment after the "bricklink:"
            config.data.yaml_set_comment_before_after_key(after=AUTH_FILE_HELP, key='bricklink')

            return True
